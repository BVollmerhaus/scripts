# Benedikt's desktop scripts

A collection of workflow, usability, and ricing scripts, primarily for use with
barebone WMs such as i3.


## Categories

* [`polybar-modules`](/polybar-modules) – Custom modules for Polybar (probably
  work with other status bars as well)

* [`wm-enhancements`](/wm-enhancements) – Scripts for enhancing barebone WMs
  and replicating common DE features


## Scripts

Some of these scripts require additional packages, which you can find listed
below or in the respective script's module docstring.

### polybar-modules

| Script | Description |
| ------ | ----------- |
[measure_ping.py](/polybar-modules/measure_ping.py) | Measures and displays the current ping, useful to check connectivity.

### wm-enhancements

| Script | Description | Python Deps | System Deps |
| ------ | ----------- | ----------- | ----------- |
[urgency_timeout.py](/wm-enhancements/urgency_timeout.py) | Automatically removes window urgency hints after a given delay. | [i3ipc](https://pypi.org/project/i3ipc/) | [wmctrl](https://www.archlinux.org/packages/community/x86_64/wmctrl/)
[battery_notify.py](/wm-enhancements/battery_notify.py) | Notifies when a battery's charge goes below a certain threshold. | | [libnotify](https://www.archlinux.org/packages/extra/x86_64/libnotify/)
[brightness_notify.py](/wm-enhancements/brightness_notify.py) | Displays a notification when the backlight brightness is changed. | [inotify_simple](https://pypi.org/project/inotify_simple/) | [libnotify](https://www.archlinux.org/packages/extra/x86_64/libnotify/)


## Dependency Installation

### Python Packages

To install the required Python packages for all of the scripts above, run the
following in the project root:
```shell
pip install --user -r requirements.txt
```

### System Packages

System packages may need to be installed separately, depending on whether your
distro bundles them.

**Arch and derivatives**
```shell
pacman -S wmctrl libnotify
```

**Debian/Ubuntu**
```shell
apt install wmctrl libnotify-bin
```
